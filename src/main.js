import Vue from 'vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './car-book.css'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import App from './App.vue'



import { router } from './routes'

Vue.component('ProjectCard', require('./components/ProjectCard.vue').default);

Vue.component('ServiceCard', require('./components/ServiceCard.vue').default);


//---------------------------------------------

Vue.prototype.$_services = [
      {id: 1, 
       title: 'Service One', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.'
      },
      {id: 2,   
       title: 'Service Two', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 3, 
       title: 'Service Three', 
       text: 'Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      } 
    ]

//------------------------------------------

//---------------------------------------------

Vue.prototype.$_projects = [
      {id: 1, 
       img: 'http://placehold.it/700x400',  
       title: 'Project One', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae?'
      },
      {id: 2, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Two', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 3, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Three', 
       text: 'Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 4, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Four', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 5, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Five', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 6, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Six', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Nihil, dolorem!'
      } 
    ]

//------------------------------------------

//------------------------------------------

Vue.config.productionTip = false

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

new Vue({
  router,	
  render: h => h(App),
}).$mount('#app')

// https://jsonplaceholder.typicode.com/
//https://vuex.vuejs.org/ru/installation.html
//https://www.youtube.com/watch?v=c2SK1IlmYL8
//https://www.youtube.com/watch?v=OlnwgS-gk8Y
//http://my-json-server.typicode.com/SergPG/car/posts/1
//https://github.com/SergPG/car
//https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch