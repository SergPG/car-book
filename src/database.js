import Vue from 'vue'

//---------------------------------------------

Vue.prototype.$_projects = [
      {id: 1, 
       img: 'http://placehold.it/700x400',  
       title: 'Project One', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 2, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Two', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 3, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Three', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 4, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Four', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 5, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Five', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      },
      {id: 6, 
       img: 'http://placehold.it/700x400',  
       title: 'Project Six', 
       text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!'
      } 
    ];

//------------------------------------------

