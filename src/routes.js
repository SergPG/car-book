import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './views/Home'
import About from './views/About'
import Portfolio from './views/Portfolio'
import Contact from './views/Contact'
import Project from './views/Project'

Vue.use(VueRouter)

export const router = new VueRouter({
	mode: 'history',
	routes: [
      { path: '', component: Home },
      { path: '/about', component: About },
      { path: '/portfolio', component: Portfolio },
      { path: '/portfolio/:id', name: 'project', component: Project },
      { path: '/contact', component: Contact }
	]
})